#import <stdio.h>
#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>

void Problem1();
void Problem2();

void Problem1()
{
   int sum = 0;
   int i = 0;
   while (i < 1000)
   {
        if (i%3 == 0 || i%5 == 0) 
        {
            sum += i;
        }
        i++;
   }
   printf("Sum of all natural numbers below one thousand that are \
   multiples of 3 or 5 is: %d\n", sum);
}

void Problem2()
{
     
       
    long int fib[400000]={400000, 0};
    long int total=2; //fib[1] is not added as the loop starts at fib[2]   
    int a=0;
    fib[0]=1;
    fib[1]=2;
    for(int a=2; a<=400000; a++)
    {
        fib[a]=fib[a-2]+fib[a-1];
        if(fib[a]>4000000)
            goto end;
        if(fib[a]%2==0)
            total+=fib[a];
    }
     
     
end:
    printf("%d\n", total);
    
}


int main()
{
  
  Problem1();
  Problem2();

  return 0;
  
}
  
